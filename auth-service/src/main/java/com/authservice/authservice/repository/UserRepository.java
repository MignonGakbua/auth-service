package com.authservice.authservice.repository;

import com.authservice.authservice.server.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User,Long>  {
    User findAllByUsername(String username);
    User findUserById(long user_id);

}
