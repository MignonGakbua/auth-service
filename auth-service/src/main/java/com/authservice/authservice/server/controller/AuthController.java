package com.authservice.authservice.server.controller;

import com.authservice.authservice.config.JwtTokenUtil;
import com.authservice.authservice.helpers.JsonLogic;
import com.authservice.authservice.helpers.JsonResult;
import com.authservice.authservice.server.domain.JwtResponse;
import com.authservice.authservice.server.domain.User;
import com.authservice.authservice.service.UserService;
import com.authservice.authservice.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.authservice.authservice.helpers.ErrorCode.*;
import static com.authservice.authservice.helpers.Logger.log;

@RequestMapping(value = "auth")
@RestController
public class AuthController {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserService userService;

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @PostMapping(value = "/signIn", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<JsonResult> login(@RequestBody String json) {
        log(getClass().getName(), "User is trying to signIn......");
        JsonResult result = new JsonResult();
        try {
            User user = (User) JsonLogic.getObject(User.class, json);
            log(getClass().getName(), "AUTHENTICATE THE USER:.....");
            authenticate(user.getUsername(), user.getPassword());
            log(getClass().getName(), "ENTERING:.....");
            User loginUser = userService.login(user);
            if (loginUser != null) {
                final UserDetails userDetails = userService.loadUserByUsername(loginUser.getUsername());
                final String token = jwtTokenUtil.generateToken(userDetails);
                result.setResult(true);
                result.setItem(new JwtResponse(token));
                result.setMessage("User is logged in successfully");
                return new ResponseEntity<>(result, HttpStatus.OK);
            } else {
                result.setResult(false);
                result.setMessage("Failed login this user.");
                result.setErrorCode(FAILED_LOG_IN);
                result.setErrorMessage("No error was returned.");
                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            result.setResult(false);
            result.setMessage("Failed to login in this user.");
            result.setErrorCode(FAILED_LOG_IN);
            result.setErrorMessage(e.toString());
            log(this.getClass(), e.getMessage());
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
    @PostMapping(value = "/signUp", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<JsonResult> register(@RequestBody String json) {
        log(getClass(), "User is trying to signUp......");
        JsonResult result = new JsonResult();
        try {
            User user = (User) JsonLogic.getObject(User.class, json);
            boolean IsValid = userValidator.validCreateUser(user);
            if (IsValid) {
                User createdUser = userService.create(user);
                if (createdUser != null) {
                    result.setResult(true);
                    result.setItem(userValidator.sanitize(createdUser));
                    result.setMessage("User is created successfully");
                    return new ResponseEntity<>(result, HttpStatus.OK);
                } else {
                    result.setResult(false);
                    result.setMessage("Failed creating this user.");
                    result.setErrorCode(FAILED_CREATING_USER);
                    result.setErrorMessage("No error was returned.");
                    return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
                }
            }
            log(getClass(), "Failed creating this user......");
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            result.setResult(false);
            result.setMessage("Failed creating this user.");
            result.setErrorCode(FAILED_CREATING_USER);
            result.setErrorMessage(e.toString());
            log(this.getClass(), e.getMessage());
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping(value = "/logout")
    public ResponseEntity<JsonResult> logoutPage(HttpServletRequest request, HttpServletResponse response) {
        log(getClass(), "User is trying to logout......");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        JsonResult result = new JsonResult();
        try {
            if (auth != null) {
                result.setResult(true);
                result.setMessage("User is successfully logout");
                request.logout();
                new SecurityContextLogoutHandler().logout(request, response, auth);
                return new ResponseEntity<>(result, HttpStatus.OK);
            } else {
                result.setResult(false);
                result.setMessage("Failed to logout.");
                result.setErrorCode(FAILED_LOG_OUT);
                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            result.setResult(false);
            result.setMessage("Failed to logout.");
            result.setErrorCode(FAILED_LOG_OUT);
            result.setErrorMessage(e.toString());
            log(this.getClass(), e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}
