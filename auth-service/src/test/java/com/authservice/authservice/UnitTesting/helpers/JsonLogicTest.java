package com.authservice.authservice.UnitTesting.helpers;

import com.authservice.authservice.helpers.JsonLogic;
import com.authservice.authservice.server.domain.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@SpringBootTest
public class JsonLogicTest {
    private static JsonLogic logic;
    private static ObjectMapper mapper;

    @Before
    public void setup() {
        mapper = new ObjectMapper();
        logic = new JsonLogic();
    }

    private void notNullAccount(User user) {
        assertNotNull(user);
        assertNotNull(user.getUsername());
        assertNotNull(user.getPassword());
    }

    private void matchAccount(User expected, User actual) {
        notNullAccount(expected);
        notNullAccount(actual);
        assertEquals(expected.getUsername(), actual.getUsername());
    }

    @Test
    public void getObject() throws JsonProcessingException {
        User user = new User();
        user.setPassword("PassGenericTest");
        user.setUsername("UserGenericTest");
        String json = mapper.writeValueAsString(user);
        User logicUser = (User) JsonLogic.getObject(User.class, json);
        matchAccount(user, logicUser);
    }

}
