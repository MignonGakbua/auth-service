//
//package com.authservice.authservice.UnitTesting.controller;
//
//import com.authservice.authservice.server.controller.AuthController;
//import com.authservice.authservice.server.domain.User;
//import com.authservice.authservice.service.UserService;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.jupiter.api.Disabled;
//import org.junit.jupiter.api.Order;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.ResultMatcher;
//
//import static com.sun.org.apache.xerces.internal.util.PropertyState.is;
//import static org.hamcrest.Matchers.hasSize;
//import static org.mockito.BDDMockito.*;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@RunWith(SpringRunner.class)
//@WebMvcTest(AuthController.class)
//@ContextConfiguration(locations ="classpath*:/spring/test-context.xml")
//@Disabled
//public class AuthControllerTest {
//
//    @Autowired
//    private MockMvc mvc;
//
//    @MockBean
//    private UserService userService;
//
//    private static ObjectMapper mapper;
//
//    private static User user;
//
//    @Before
//    public void setup()
//    {
//        mapper = new ObjectMapper();
//        user = new User();
//        user.setId(0l);
//        user.setRole("ADMIN");
//        user.setUsername("GenericUserNameTest");
//        user.setPassword("GenericPasswordTest");
//
//    }
//
//    @Disabled
//    @Test
//    @Order(1)
//    public void givenSignUp_WhenSignUpWithUserReturnJsonResult()
//            throws Exception {
//
//        String json = mapper.writeValueAsString(user);
//        given(userService.create(user)).willReturn(user);
//        mvc.perform(post("auth/signUp").content(json)
//                .content(MediaType.APPLICATION_JSON_VALUE))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$", hasSize(1)));
////                .andExpect(jsonPath("$[0].username", is(user.getUsername())));
//
//    }
//
//    @Disabled
//    @Test
//    @Order(2)
//    public void givenSignIn_WhenSignInWithUserReturnJsonResult()
//            throws Exception {
//
//
//        String json = mapper.writeValueAsString(user);
//        given(userService.create(user)).willReturn(user);
//        mvc.perform(post("auth/sigIn").content(json)
//                .content(MediaType.APPLICATION_JSON_VALUE))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$", hasSize(1)))
//                .andExpect((ResultMatcher) jsonPath("$[0].username", is(user.getUsername())));
//    }
//
//}
//
