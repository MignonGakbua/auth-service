package com.authservice.authservice.UnitTesting.helpers;

import com.authservice.authservice.helpers.Logger;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.boot.test.context.SpringBootTest;

import static com.authservice.authservice.helpers.Logger.log;


@SpringBootTest
public class LoggerTest {


    @Test
    public void logStringSourceMessage() {
        Logger.log("Test", "");
    }
    @Test
    public void logStringSourceExceptionMessage() {
        Logger.log("Test", "");
    }

    @Test
    public void logClassSourceMessage() {
        Logger.log(this.getClass().getName(), "Test this log by name ");
    }

    @Test
    public void logClassSourceExceptionMessage() {
        Logger.log(this.getClass(), "Everything broke again");
    }

    @Test
    public void logClassSourceExceptionByErrorNullPointerException()
    {
        try{
            String empty = null;
            String not = empty.substring(0);
        }
        catch (Exception e) {
            log(this.getClass(), true, e);
        }
    }

    @Test
    public void logClassSourceExceptionByErrorNullPointerException2()
    {
        try{
            String empty = null;
            String not = empty.substring(1);
        }
        catch (Exception e) {
            log(this.getClass(), true, e);
        }
    }

}
