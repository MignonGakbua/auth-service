package com.authservice.authservice.UnitTesting.helpers;

import com.authservice.authservice.helpers.ErrorCode;
import com.authservice.authservice.helpers.JsonResult;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class JsonResultTest {

    private static JsonResult jsonResult;
    @BeforeEach
    void setUp() {
        jsonResult = new JsonResult();
    }



    @Test
    void getSetMessage() {
        String message = "UnitTest";
        jsonResult.setMessage(message);
        assertEquals(message, jsonResult.getMessage());
    }

    @Test
    void getSetNullMessage() {
        String message = null;
        jsonResult.setMessage(message);
        assertEquals(message, jsonResult.getMessage());
    }

    @Test
    void getSetResultTrue() {
        boolean result = true;
        jsonResult.setResult(result);
        assertEquals(result, jsonResult.getResult());
    }

    @Test
    void getSetResultFalse() {
        boolean result = false;
        jsonResult.setResult(result);
        assertEquals(result, jsonResult.getResult());
    }

    @Test
    void getSetItem() {
        Object object = new Object();
        jsonResult.setItem(object);
        assertEquals(object, jsonResult.getItem());
    }

    @Test
    void getSetItemNull() {
        Object object = null;
        jsonResult.setItem(object);
        assertEquals(object, jsonResult.getItem());
    }

    @Test
    void SetGetErrorCodeOnNumber() {
        int code = 2001;
        jsonResult.setErrorCode(code);
        assertEquals(ErrorCode.valueOf(code), jsonResult.getErrorEnum());
        assertEquals(code, jsonResult.getErrorCode());
    }

    @Test
    void SetGetErrorCodeOnEnum() {
        int code = 3001;
        jsonResult.setErrorCode(ErrorCode.valueOf(code));
        assertEquals(ErrorCode.valueOf(code), jsonResult.getErrorEnum());
        assertEquals(code, jsonResult.getErrorCode());
    }

    @Test
    void GetSetErrorMessage() {
        String message = "UnitTest";
        jsonResult.setMessage(message);
        assertEquals(message, jsonResult.getMessage());
    }

}
