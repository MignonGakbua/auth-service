package com.authservice.authservice.IntegrationTesting.service;

import com.authservice.authservice.repository.UserRepository;
import com.authservice.authservice.server.domain.User;
import com.authservice.authservice.service.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class UserServiceIT {

    @TestConfiguration
    static class UserServiceImplTestContextConfiguration {
        @Bean
        public UserService userService() {
            return new UserService();
        }
    }

    @Autowired
    private UserService userService;

    @MockBean
    public UserRepository userRepository;

    private static User user;


    @Before
    public void whenSaved_thenFindsByNameForMock() {
        user = new User();
        user.setUsername("GenericUserNameTest");
        user.setId(2l);
        user.setPassword("GenericPasswordTest");
        user.setRole("ADMIN");

        Mockito.when(userRepository.findAllByUsername(user.getUsername()))
                .thenReturn(user);

        Mockito.when(userRepository.findUserById(user.getId()))
                .thenReturn(user);
    }

    @Test
    public void whenValidUserName_thenUserShouldBeFound() {
        String name = user.getUsername();
        User found = userService.findAllByUsername(name);
        Assert.assertEquals(found.getUsername().trim(),name.trim());
    }

    @Test
    public void whenValidId_thenUserShouldBeFound() {
        long userId = user.getId();
        String name = user.getUsername();
        User found = userService.findUserById(userId);
        Assert.assertEquals(found.getUsername().trim(),name.trim());
    }

}
