//package com.authservice.authservice.IntegrationTesting.controller;
//
//import com.authservice.authservice.server.controller.AuthController;
//import com.authservice.authservice.server.domain.User;
//import com.authservice.authservice.service.UserService;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.jupiter.api.Disabled;
//import org.junit.runner.RunWith;
//import org.mockito.Mockito;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.ResultMatcher;
//
//import static org.assertj.core.internal.bytebuddy.matcher.ElementMatchers.is;
//import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
//import static org.springframework.test.web.client.match.MockRestRequestMatchers.jsonPath;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@RunWith(SpringRunner.class)
//@ContextConfiguration(locations ="classpath*:/spring/test-context.xml")
//@WebMvcTest(AuthController.class)
//@Disabled
//public class AuthControllerIntegrationIT {
//
//    @Autowired
//    private MockMvc mvc;
//
//    @MockBean
//    private UserService userService;
//
//    private static ObjectMapper mapper;
//
//    private static User user;
//
//    @Before
//    public void setup() {
//        user = new User();
//        mapper = new ObjectMapper();
//    }
//
//
//    public void createUser(String username, String password) {
//        user.setUsername(username);
//        user.setPassword(password);
//        user.setRole("USER");
//        Mockito.when(userService.create(user)).thenReturn(user);
//
//    }
//
//    @Disabled
//    @Test
//    public void createUserWhenReturnUserThatCreated() throws Exception {
//
//        this.createUser("GenericUsernameTest","GenericPasswordTest");
//
//        String json = mapper.writeValueAsString(user);
//        mvc.perform(post("/auth/signUp")
//                .content(json)
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect((ResultMatcher) content()
//                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
//                .andExpect((ResultMatcher) jsonPath("$[0].username", is("GenericUsernameTest")));
//
//    }
//
//
//}
